// ---------------------------------------------------------------------------
//  object-has-property
//
//  hasOwnProperty check
//  - object: Object to check
//  - property: Property to find
// ---------------------------------------------------------------------------

    var hasOwnProperty = Object.prototype.hasOwnProperty;

    function objectHasProperty(object, key) {
        return hasOwnProperty.call(object, key);
    }

    module.exports = objectHasProperty;
